/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-animation
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include <SDL/SDL.h>

#include "animate.h"

void animate::OnAnimate() {
	if(OldTime + FrameRate > SDL_GetTicks()) {
		return;
	}

	OldTime = SDL_GetTicks();

	CurrentFrame += FrameInc;

	if(Oscillate) {
		if(FrameInc > 0) {
			if(CurrentFrame >= MaxFrames) {
				FrameInc = -FrameInc;
			}
		} else if (CurrentFrame <= 0) {
			FrameInc = -FrameInc;
		}
	} else if (CurrentFrame >= MaxFrames) {
		CurrentFrame = 0;
	}
}

void animate::SetFrameRate(int Rate) {
	FrameRate = Rate;
}

void animate::SetCurrentFrame(int Frame) {
	if(Frame < 0 || Frame >= MaxFrames) return;

	CurrentFrame = Frame;
}

int animate::GetCurrentFrame() {
	return CurrentFrame;
}	
