/**
 * SDL collision exercise.
 * 
 * Reference : http://www.sdltutorials.com/sdl-collision
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _FPS_H_
#define _FPS_H_

class fps {
	public:
		fps() {
			old_time = 0;
			last_time = 0;

			frame_rate = 0;

			frames = 0;
			num = 0;
		}

		static fps fps_control;

		void loop();
		int get_frames_per_second();
		float get_frame_rate();

	private:
		int old_time;
		int last_time;

		float frame_rate;

		int num;
		int frames;
};

#endif // _FPS_H_
