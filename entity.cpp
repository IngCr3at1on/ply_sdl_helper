/**
 * SDL Entities exercise.
 * 
 * Reference : http://www.sdltutorials.com/sdl-entities
 *             http://www.sdltutorials.com/sdl-collision
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "entity.h"

std::vector<entity*> entity::EntityList;
std::vector<entitycol> entitycol::EntityColList;

entity::entity() {
	entity_surface = NULL;
	
	x = 0;
	y = 0;

	width = 0;
	height = 0;

	move_left = false;
	move_right = false;

	type = ENTITY_TYPE_GENERIC;

	dead = false;
	flags = ENTITY_FLAG_GRAVITY;

	speed_x = 0;
	speed_y = 0;

	accel_x = 0;
	accel_y = 0;

	max_speed_x = 5;
	max_speed_y = 5;

	cur_col = 0;
	cur_row = 0;

	collision_x = 0;
	collision_y = 0;
	collision_w = 0;
	collision_h = 0;
}

bool entity::new_entity(const char *file, int width, int height, int frames) {
	if((entity_surface = surface::new_surface(file)) == NULL) {
		return false;
	}

	this->width = width;
	this->height = height;

	Anim_Control.MaxFrames = frames;

	return true;
}

// Added to support transparent func with bitmaps.

bool entity::new_entity_from_bitmap(const char *file, int width, int height, int frames) {
	if((entity_surface = surface::new_bitmap(file)) == NULL) {
		return false;
	}

	surface::set_transparent(entity_surface, 255, 0, 255);

	this->width = width;
	this->height = height;

	Anim_Control.MaxFrames = frames;

	return true;
}

void entity::loop() {
	if(move_left == false && move_right == false) {
		stop_move();
	}

	if(move_left) {
		accel_x = -0.5;
	} else if (move_right) {
		accel_x = 0.5;
	}

	if(flags & ENTITY_FLAG_GRAVITY) {
		accel_y = 0.75f;
	}

	speed_x += accel_x * fps::fps_control.get_frame_rate();
	speed_y += accel_y * fps::fps_control.get_frame_rate();

	if(speed_x > max_speed_x) {
		speed_x = max_speed_x;
	}
	if(speed_x < -max_speed_x) {
		speed_x = -max_speed_x;
	}
	if(speed_y > max_speed_y) {
		speed_y = max_speed_y;
	}
	if(speed_y < -max_speed_y) {
		speed_y = -max_speed_y;
	}

	animate();
	move(speed_x, speed_y);
}

void entity::render(SDL_Surface *display) {
	if(entity_surface == NULL || display == NULL) return;

	surface::draw_surface(
		display,
		entity_surface,
		x - CCamera::CameraControl.GetX(),
		y - CCamera::CameraControl.GetY(),
		cur_col * width,
		(cur_row + Anim_Control.GetCurrentFrame()) * height,
		width,
		height
	);
}

void entity::animation() {
	if(move_left) {
		cur_col = 0;
	} else if (move_right) {
		cur_col = 1;
	}
	Anim_Control.OnAnimate();
}

void entity::move(float move_x, float move_y) {
	if(move_x == 0 && move_y == 0)
		return;

	double new_x = 0;
	double new_y = 0;

	move_x *= fps::fps_control.get_frame_rate();
	move_y *= fps::fps_control.get_frame_rate();

	if(move_x != 0) {
		if(move_x >= 0) {
			new_x = fps::fps_control.get_frame_rate();
		} else {
			new_x = -fps::fps_control.get_frame_rate();
		}
	}
	if(move_y != 0) {
		if(move_y >= 0) {
			new_y = fps::fps_control.get_frame_rate();
		} else {
			new_y = -fps::fps_control.get_frame_rate();
		}
	}

	while(true) {
		if(flags && ENTITY_FLAG_GHOST) {
			PosValid((int)(x + new_x), (int)(y + new_y));

			x += new_x;
			y += new_y;
		} else {
			if(PosValid((int)(x + new_x), (int)(y))) {
				y += new_y;
			} else {
				speed_x = 0;
			}
	
			if(PosValid((int)(x), (int)(y + new_y))) {
				y += new_y;
			} else {
				speed_y = 0;
			}
		}

		move_x += -new_x;
		move_y += -new_y;

		if(new_x > 0 && move_x <= 0) {
			new_x = 0;
		}
		if(new_x < 0 && move_x >= 0) {
			new_x = 0;
		}

		if(new_y > 0 && move_y <= 0) {
			new_y = 0;
		}
		if(new_y < 0 && move_y >= 0) {
			new_y = 0;
		}

		if(move_x == 0) {
			new_x = 0;
		}
		if(move_y == 0) {
			new_y = 0;
		}

		if(move_x == 0 && move_y == 0) {
			break;
		}
		if(new_x == 0 && new_y == 0) {
			break;
		}
	}
}

void entity::stop_move() {
	if(speed_x > 0) {
		accel_x = -1;
	}

	if(speed_x < 0) {
		accel_x = 1;
	}

	if(speed_x < 2.0f && speed_x > -2.0f) {
		accel_x = 0;
		speed_x = 0;
	}
}

bool entity::collision(int ox, int oy, int ow, int oh) {
	int left1, left2;
	int right1, right2;
	int top1, top2;
	int bottom1, bottom2;

	int tX = (int)x + collision_x;
	int tY = (int)y + collision_y;

	left1 = tX;
	left2 = ox;

	right1 = left1 + width - 1 - collision_w;
	right2 = ox + ow - 1;

	top1 = tY;
	top2 = oy;

	bottom1 = top1 + height - 1 - collision_h;
	bottom2 = oy + oh - 1;

	if(bottom1 < top2) return false;
	if(top1 > bottom2) return false;

	if(right1 < left2) return false;
	if(left1 > right2) return false;

	return true;
}

bool entity::PosValid(int new_x, int new_y) {
	bool ret = true;

	int startX = (new_x + collision_x) / TILE_SIZE;
	int startY = (new_y + collision_y) / TILE_SIZE;

	int endX = ((new_x + collision_x) + width - collision_w - 1) / TILE_SIZE;
	int endY = ((new_y + collision_y) + height - collision_h - 1) / TILE_SIZE;

	for(int iY = startY; iY <= endY; iY++) {
		for(int iX = startX; iX <= endX; iX++) {
			CTile *tile = CArea::AreaControl.GetTile(iX * TILE_SIZE, iY * TILE_SIZE);

			if(PosValidTile(tile) == false) {
				ret = false;
			}
		}
	}

	if(flags & ENTITY_FLAG_MAPONLY) {
	} else {
		for(int i = 0; i < EntityList.size(); i++) {
			if(PosValidEntity(EntityList[i], new_x, new_y) == false) {
				ret = false;
			}
		}
	}

	return ret;
}

bool entity::PosValidTile(CTile *tile) {
	if(tile != NULL && tile->TypeID == TILE_TYPE_BLOCK) {
		return false;
	}

	return true;
}

bool entity::PosValidEntity(entity *entity, int new_x, int new_y) {
	if(this != entity && entity != NULL && entity->dead == false &&
		entity->flags ^ ENTITY_FLAG_MAPONLY &&
		entity->collision(
			new_x + collision_x,
			new_y + collision_y,
			width - collision_w - 1,
			height - collision_h - 1
		) == true)
	{
		entitycol col;

		col.EntityA = this;
		col.EntityB = entity;

		entitycol::EntityColList.push_back(col);

		return false;
	}

	return true;
}

void entity::cleanup() {
	if(entity_surface) {
		SDL_FreeSurface(entity_surface);
	}

	entity_surface = NULL;
}
