/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-animation
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _ANIMATE_H_
#define _ANIMATE_H_

class animate {
	public:
		animate() {
			CurrentFrame = 0;
			MaxFrames = 0;
			FrameInc = 1;

			FrameRate = 100; // Milliseconds
			OldTime = 0;

			Oscillate = false;
		}	

		void OnAnimate();

		void SetFrameRate(int Rate);
		void SetCurrentFrame(int Frame);
		int GetCurrentFrame();

		int MaxFrames;
		bool Oscillate;

	private:
		int CurrentFrame;
		int FrameInc;

		int FrameRate;
		long OldTime;
};

#endif // _ANIMATE_H_
	
