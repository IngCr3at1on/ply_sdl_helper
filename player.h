/**
 * SDL Entities exercise.
 * 
 * Reference : http://www.sdltutorials.com/sdl-collision
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "entity.h"

class Player : public entity {
	public:
		Player() { }
	
		bool OnLoad(const char *file, int width, int height, int frames) {
			if(entity::new_entity(file, width, height, frames) == false) {
				return false;
			}
			return true;
		}
		void loop() {
			entity::loop();
		}
		void render(SDL_Surface *display) {
			entity::render(display);
		}
		void animate() {
			if(sx != 0) {
				Anim_Control.MaxFrames = 8;
			} else {
				Anim_Control.MaxFrames = 0;
			}
			entity::animation();
		}
		void collision(entity *entity) { }
		void cleanup() {
			entity::cleanup();
		}
};

#endif // _CPLAYER_H_
