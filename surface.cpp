/**
 * Generic input handling for usage in SDL programming.
 * 
 * Reference : http://www.sdltutorials.com/
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "surface.h"

// Load a new image.
SDL_Surface* surface::new_surface(const char *file) {
	SDL_Surface *tmp = NULL;
	SDL_Surface *ret = NULL;
	/* Load any image into memory (note this breaks transparency function for
	 * bitmaps). */
	if((tmp = IMG_Load(file)) == NULL) {
		return NULL;
	}
	ret = SDL_DisplayFormatAlpha(tmp);
	SDL_FreeSurface(tmp);
	return ret;
}

// Added to keep support for transparency function for bitmaps.
SDL_Surface* surface::new_bitmap(const char *file) {
	SDL_Surface *tmp = NULL;
	SDL_Surface *ret = NULL;
	// Load a bitmap into memory.
	if((tmp = SDL_LoadBMP(file)) == NULL) {
		return NULL;
	}
	ret = SDL_DisplayFormat(tmp);
	SDL_FreeSurface(tmp);
	return ret;
}

bool surface::draw_surface(SDL_Surface *dest,
						SDL_Surface *source,
						int x,
						int y) 
{
	if(dest == NULL || source == NULL) {
		return false;
	}

	SDL_Rect destr;

	destr.x = x;
	destr.y = y;

	SDL_BlitSurface(source, NULL, dest, &destr);

	return true;
}

bool surface::draw_surface(SDL_Surface *dest,
						SDL_Surface *source,
						int x,
						int y,
						int x2,
						int y2,
						int w,
						int h)
{
	if(dest == NULL || source == NULL) {
		return false;
	}

	SDL_Rect destr;

	destr.x = x;
	destr.y = y;

	SDL_Rect sourcer;

	sourcer.x = x2;
	sourcer.y = y2;
	sourcer.w = w;
	sourcer.h = h;

	SDL_BlitSurface(source, &sourcer, dest, &destr);

	return true;
}

bool surface::set_transparent(SDL_Surface *dest, int r, int g, int b) {
	if(dest == NULL) {
		return false;
	}

	SDL_SetColorKey(dest, SDL_SRCCOLORKEY | SDL_RLEACCEL, SDL_MapRGB(dest->format, r, g, b));

	return true;
}

/* Solid color changer (pass the RGB color code for the color you want to change
 * followed by the RGB code for the color to change to), return true on success.
 */
bool surface::change_solid_clr(SDL_Surface *dest,
							int old_r, int old_g, int old_b,
							int new_r, int new_g, int new_b) {
	if(dest == NULL) {
		printf("surface::change_solir_clr: NULL surface.\n");
		return false;
	}
	// Grab all the pixels in the passed surface
	Uint8 *pixels = (Uint8 *) dest->pixels;
	// Run through each pixel and check it against color code 1
	for(int i = 0; i >= sizeof(pixels); i++) {
		if(pixels[i] == SDL_MapRGB(dest->format, old_r, old_g, old_b)) {
			// If the color matches change that pixel to the new color code.
			pixels[i] = SDL_MapRGB(dest->format, new_r, new_g, new_b);
		}
	}

	return true;
}
