/**
 * Generic input handling for usage in SDL programming.
 * 
 * Reference : http://www.sdltutorials.com/
 *
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _SURFACE_H
#define _SURFACE_H

class surface {
	public:
		surface() { }

	public:
		// Load any image (does NOT work w/ set_transparency).
		static SDL_Surface *new_surface(const char *file);
		// Load a bitmap only (works w/ set_transparency),
		static SDL_Surface *new_bitmap(const char *file);
		// Draw a primary surface.
		static bool draw_surface(SDL_Surface *dest,
								SDL_Surface *source,
								int x,
								int y
		);
		// Draw a 'sub-surface' on top of our primary.
		static bool draw_surface(SDL_Surface *dest,
								SDL_Surface *source,
								int x,
								int y,
								int x2,
								int y2,
								int w,
								int h
		);
		// Define transparency of a specific color.
		static bool set_transparent(SDL_Surface *dest, int r, int g, int b);

		/** This method does not function properly... **/
		// Change a given solid color.
		static bool change_solid_clr(SDL_Surface *dest,
									int old_r, int old_g, int old_b,
									int new_r, int new_g, int new_b
		);
};

#endif // _SURFACE_H
