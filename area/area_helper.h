/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _AREA_HELPER_H_
#define _AREA_HELPER_H_

// Include the general headers which are needed in map/area projects
#include "Define.h"
#include "CArea.h"
#include "CCamera.h"

#endif // _AREA_HELPER_H_
