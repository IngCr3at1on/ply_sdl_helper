/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "CCamera.h"

CCamera CCamera::CameraControl;

void CCamera::OnMove(int mx, int my) {
	x += mx;
	y += my;
}

int CCamera::GetX() {
	if(targx != NULL) {
		if(mode == TARGET_MODE_CENTER) {
			return *targx - (WWIDTH / 2);
		}
		return *targx;
	}
	return x;
}

int CCamera::GetY() {
	if(targy != NULL) {
		if(mode == TARGET_MODE_CENTER) {
			return *targy - (WHEIGHT / 2);
		}
		return *targy;
	}
	return y;
}

void CCamera::SetPos(int x, int y) {
	this->x = x;
	this->y = y;
}

void CCamera::SetTarget(float *x, float *y) {
	targx = x;
	targy = y;
}
