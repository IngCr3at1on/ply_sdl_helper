/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _CMAP_H_
#define _CMAP_H_

#include <SDL/SDL.h>
#include <vector>

#include "../surface.h"
#include "CTile.h"

class CMap {
	public:
		CMap() {
			tileset = NULL;
		}

		bool OnLoad(const char* file);
		void OnRender(SDL_Surface *display, int mapx, int mapy);

		SDL_Surface *tileset;

		CTile *GetTile(int x, int y);

	private:
		std::vector<CTile> TileList;
};

#endif // _CMAP_H_
