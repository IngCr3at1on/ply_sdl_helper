/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _CCAMERA_H_
#define _CCAMERA_H_

#include <SDL/SDL.h>

#include "Define.h"

enum {
	TARGET_MODE_NORMAL,
	TARGET_MODE_CENTER
};

class CCamera {
	public:
		CCamera() {
			x = 0;
			y = 0;

			targx = NULL;
			targy = NULL;

			mode = TARGET_MODE_NORMAL;
		}

		static CCamera CameraControl;

		void SetPos(int x, int y);
		void SetTarget(float *x, float *y);
		void OnMove(int mx, int my);

		int mode;
		int GetX();
		int GetY();
	
	private:
		int x;
		int y;
		float *targx;
		float *targy;
};

#endif // _CCAMERA_H_
