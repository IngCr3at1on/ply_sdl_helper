/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _CTILE_H_
#define _CTILE_H_

#include "Define.h"

enum {
	TILE_TYPE_NONE,
	TILE_TYPE_NORMAL,
	TILE_TYPE_BLOCK
};

class CTile {
	public:
		CTile() {
			TileID = 0;
			TypeID = TILE_TYPE_NONE;
		}

		int TileID;
		int TypeID;
};

#endif // _CTILE_H_
