/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "CMap.h"

bool CMap::OnLoad(const char *file) {
	TileList.clear();

	FILE* FileHandle = fopen(file, "r");

	if(FileHandle == NULL) {
		return false;
	}

	for(int Y = 0; Y < MAP_HEIGHT; Y++) {
		for(int X = 0; X < MAP_WIDTH; X++) {
			CTile tmp;

			fscanf(FileHandle, "%d:%d", &tmp.TileID, &tmp.TypeID);

			TileList.push_back(tmp);
		}
		fscanf(FileHandle, "\n");
	}
	fclose(FileHandle);

	return true;
}

void CMap::OnRender(SDL_Surface *display, int mapx, int mapy) {
	if(tileset == NULL) return;

	int TilesetWidth = tileset->w / TILE_SIZE;
	int TilesetHeight = tileset->h / TILE_SIZE;

	int ID = 0;

	for(int Y = 0; Y < MAP_HEIGHT; Y++) {
		for(int X = 0; X < MAP_WIDTH; X++) {
			if(TileList[ID].TypeID = TILE_TYPE_NONE) {
				ID++;
				continue;
			}

			int tX = mapx + (X * TILE_SIZE);
			int tY = mapy + (Y * TILE_SIZE);

			int tilesetX = (TileList[ID].TileID % TilesetWidth) * TILE_SIZE;
			int tilesetY = (TileList[ID].TileID / TilesetWidth) * TILE_SIZE;

			surface::draw_surface(display, tileset, tX, tY, tilesetX, tilesetY, TILE_SIZE, TILE_SIZE);

			ID++;
		}
	}
}

CTile *CMap::GetTile(int x, int y) {
	int ID = 0;

	ID = x / TILE_SIZE;
	ID = ID + (MAP_WIDTH * (y / TILE_SIZE));

	if(ID < 0 || ID >+ TileList.size()) {
		return NULL;
	}

	return &TileList[ID];
}
