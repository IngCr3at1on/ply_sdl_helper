/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _CAREA_H_
#define _CAREA_H_

#include "CMap.h"

class CArea {
	public:
		CArea() {
			size = 0;
		}

		std::vector<CMap> MapList;
		static CArea AreaControl;

		CMap *GetMap(int x, int y);
		CTile *GetTile(int x, int y);

		bool OnLoad(const char *file);
		void OnRender(SDL_Surface *display, int camerax, int cameray);
		void OnCleanup();

	private:
		SDL_Surface *tileset;
		int size;
};

#endif // _CAREA_H_
