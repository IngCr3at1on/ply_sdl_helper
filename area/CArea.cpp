/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "CArea.h"

CArea CArea::AreaControl;

bool CArea::OnLoad(const char *file) {
	MapList.clear();

	FILE* FileHandle = fopen(file, "r");

	if(FileHandle == NULL) {
		return false;
	}

	char TilesetFile[255];

	fscanf(FileHandle, "%s\n", TilesetFile);

	if((tileset = surface::new_surface(TilesetFile)) == false) {
		fclose(FileHandle);
		return false;
	}

	fscanf(FileHandle, "%s\n", &size);

	for(int X = 0; X < size; X++) {
		for(int Y = 0; Y < size; Y++)  {
			char MapFile[255];

			fscanf(FileHandle, "%s ", MapFile);

			CMap tmp;
			if(tmp.OnLoad(MapFile) == false) {
				fclose(FileHandle);
				return false;
			}

			tmp.tileset = tileset;
			MapList.push_back(tmp);
		}
		fscanf(FileHandle, "\n");
	}
	fclose(FileHandle);
	return true;
}

void CArea::OnRender(SDL_Surface *display, int camerax, int cameray) {
	int MapWidth = MAP_WIDTH * TILE_SIZE;
	int MapHeight = MAP_HEIGHT * TILE_SIZE;

	int FirstID = -camerax / MapWidth;
	FirstID = FirstID + ((-cameray / MapHeight) * size);

	for(int i = 0; i < 4; i++) {
		int ID = FirstID + ((i / 2) * size) + (i % 2);

		if(ID < 0 || ID >= MapList.size()) continue;

		int X = ((ID % size) * MapWidth) + camerax;
		int Y = ((ID / size) * MapHeight) + cameray;

		MapList[ID].OnRender(display, X, Y);
	}
}

void CArea::OnCleanup() {
	if(tileset) {
		SDL_FreeSurface(tileset);
	}
	MapList.clear();
}

CMap *CArea::GetMap(int x, int y) {
	int MapWidth = MAP_WIDTH * TILE_SIZE;
	int MapHeight = MAP_HEIGHT * TILE_SIZE;

	int ID = x / MapWidth;
	ID = ID + ((y / MapHeight) * size);

	if(ID < 0 || ID >= MapList.size()) {
		return NULL;
	}

	return &MapList[ID];
}

CTile *CArea::GetTile(int x, int y) {
	int MapWidth = MAP_WIDTH * TILE_SIZE;
	int MapHeight = MAP_HEIGHT * TILE_SIZE;

	CMap *map = GetMap(x, y);

	if(map == NULL) {
		return NULL;
	}

	x = x % MapWidth;
	y = y % MapHeight;

	return map->GetTile(x, y);
}
