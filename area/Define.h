/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _DEFINE_H_
#define _DEFINE_H_

#define MAP_WIDTH 40
#define MAP_HEIGHT 40

#define TILE_SIZE 16

#define WWIDTH 640
#define WHEIGHT 480

#endif // _DEFINE_H_
