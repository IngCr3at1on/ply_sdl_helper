/**
 * SDL Entities exercise.
 * 
 * Reference : http://www.sdltutorials.com/sdl-entities
 *             http://www.sdltutorials.com/sdl-collision
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <vector>

#include "area/area_helper.h"
#include "animate.h"
#include "fps.h"
#include "surface.h"

enum {
	ENTITY_TYPE_GENERIC,
	ENTITY_TYPE_PLAYER
};

enum {
	ENTITY_FLAG_NONE		= 0,
	ENTITY_FLAG_GRAVITY		= 0x00000001,
	ENTITY_FLAG_GHOST		= 0x00000002,
	ENTITY_FLAG_MAPONLY		= 0x00000004
};

class entity {
	public:
		entity();
		virtual ~entity() { }

		virtual bool new_entity(const char *file,
								int width,
								int height,
								int frames
		);
		virtual bool new_entity_from_bitmap(const char *file,
											int width,
											int height,
											int frames
		);

		virtual void loop();
		virtual void render(SDL_Surface *display);
		virtual void animation();
		virtual void collission(entity *entity) { }
		virtual void cleanup();

		static std::vector<entity*> EntityList;

		void move(float move_x, float move_y);
		void stop_move();
		bool collision(int ox, int oy, int ow, int oh);

		float x;
		float y;

		int width;
		int height;

		bool move_left;
		bool move_right;

		int type;

		bool dead;
		int flags;

		float max_speed_x;
		float max_speed_y;

	protected:
		animate Anim_Control;
		SDL_Surface *entity_surface;

		float speed_x;
		float speed_y;
		float accel_x;
		float accel_y;

		int cur_col;
		int cur_row;
		int collision_x;
		int collision_y;
		int collision_w;
		int collision_h;

	private:
		bool PosValid(int nx, int ny);
		bool PosValidTile(CTile *tile);
		bool PosValidEntity(entity *entity, int nx, int ny);
};

class entitycol {
	public:
		entitycol() {
			EntityA = NULL;
			EntityB = NULL;
		}

		static std::vector<entitycol> EntityColList;

		entity *EntityA;
		entity *EntityB;
};

#endif // _ENTITY_H_
