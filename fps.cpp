/**
 * SDL collision exercise.
 * 
 * Reference : http://www.sdltutorials.com/sdl-collision
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include <SDL/SDL.h>

#include "fps.h"

fps fps::fps_control;

void fps::loop() {
	// Restart fps count every second.
	if(old_time + 1000 < SDL_GetTicks()) {
		old_time = SDL_GetTicks();

		num = frames;

		frames = 0;
	}

	frame_rate = ((SDL_GetTicks() - last_time) / 1000.0f) * 32.0f;

	last_time = SDL_GetTicks();

	frames++;
}

int fps::get_frames_per_second() {
	return num;
}

float fps::get_frame_rate() {
	return frame_rate;
}
